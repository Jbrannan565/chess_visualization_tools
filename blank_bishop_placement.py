#! /usr/bin/env python3
import chess
from random import randint

board = chess.Board()

board.clear()

loc = randint(0,63)

print("The bishop is on:", chess.SQUARE_NAMES[loc])

edge_squares = []

for i in range(64):
    if i <= 6 and i != 0:
        edge_squares.append(i)
    if i >= 57 and i != 63:
        edge_squares.append(i)
    if (i % 8 == 0) or ((i+1) % 8 == 0):
        edge_squares.append(i)

board.set_piece_at(loc, chess.Piece(chess.BISHOP, chess.WHITE))

farthest_moves = []

for move in board.legal_moves:
    if move.to_square in edge_squares:
        farthest_moves.append(move.uci()[2:])

guesses = input("> ")
guesses = guesses.lower().split(" ")
guesses.sort()
farthest_moves.sort()

'''
if guesses == farthest_moves:
    print("Correct!")
else:
    print("Incorrect.\n")
'''
print("correct answer:", " ".join(farthest_moves))
