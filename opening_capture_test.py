#! /usr/bin/env python3

import chess
import chess.pgn
from random import randint

pgn = open("pgns/Carlsen.pgn", "r")

offsets = []

while True:
    offset = pgn.tell()

    headers = chess.pgn.read_headers(pgn)
    
    if headers is None:
        break

    offsets.append(offset)

loc = randint(0, len(offsets) - 1)

pgn.seek(offsets[loc])

game = chess.pgn.read_game(pgn)

board = chess.Board()

moves_iterable = []

c = randint(8, 17)

moves = ""

for idx, move in enumerate(game.mainline_moves()):
    if idx % 2 == 0:
        moves += "{}. ".format(board.fullmove_number)
        moves += "{}".format(board.san(move))
    else:
        moves += " {}\n".format(board.san(move))

    board.push(move)
    moves_iterable.append(move)

    c = c - 1
    if (c == 0):
        break

print(moves)

captures = []
for move in board.legal_moves:
    if board.is_capture(move):
        captures.append(board.san(move))

guesses = input("Enter all legal captures (ex: Bxa4). >").strip()

if guesses == '':
    guesses = []
else:
    guesses = guesses.split(" ")

guesses.sort()
captures.sort()

if guesses == captures:
    print("Correct!")
else:
    print("Incorrect.")

print(", ".join(captures))
print(board)
