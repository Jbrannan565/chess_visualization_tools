#! /usr/bin/env python3

import chess
import chess.pgn
from random import randint

pgn = open("pgns/Carlsen.pgn", "r")

offsets = []

while True:
    offset = pgn.tell()

    headers = chess.pgn.read_headers(pgn)
    
    if headers is None:
        break

    offsets.append(offset)

loc = randint(0, len(offsets) - 1)

pgn.seek(offsets[loc])

print(chess.pgn.read_game(pgn))
