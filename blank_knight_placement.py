#! /usr/bin/env python3
import chess
from random import randint

board = chess.Board()

board.clear()

loc = randint(0,63)

print("The night is on:", chess.SQUARE_NAMES[loc])

board.set_piece_at(loc, chess.Piece(chess.KNIGHT, chess.WHITE))

legal_moves = []

for move in board.legal_moves:
    legal_moves.append(move.uci()[2:])

guesses = input("> ")
guesses = guesses.lower().split(" ")
guesses.sort()
legal_moves.sort()

'''
if guesses == legal_moves:
    print("Correct!")
else:
    print("Incorrect.\n")
'''
print("correct answer:", " ".join(legal_moves))
